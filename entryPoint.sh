#!/bin/sh

# Let op!!!!!
# De line ending van dit document moet Unix (LF) zijn!!
# Ander resulteerd dit in de volgende error: "standard_init_linux.go:207: exec user process caused "no such file or directory""
# De file heeft, wanneer uit gecheckt, line ending Windows (CR LF)
# Gebruik NotePad++ > Edit > EOL Conversion > Unix (LF)

set -xe

sed -i "s/APIURL/${APIURL:-''}/g" /usr/share/nginx/html/env.js
sed -i "s/CLIENTID/${CLIENTID:-''}/g" /usr/share/nginx/html/env.js

nginx -g "daemon off;"
