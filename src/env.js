(function (window) {
  window.__env = window.__env || {};

  // API url - defaults to url of localhost when env is not set
  apiUrl = "APIURL"
  window.__env.apiUrl = apiUrl.length > 6 ? apiUrl : 'http://localhost:8080/api';

  // Client ID - defaults to clientId of localhost when env is not set
  clientId = "CLIENTID"
  window.__env.clientId = clientId.length > 8 ? clientId : 'b61f2ffb-b60a-4f9b-8908-bb6b3debc562';
}(this));
