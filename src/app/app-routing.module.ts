import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CursusListComponent } from './component/cursus-list/cursus-list.component';
import { CursusDetailComponent } from './component/cursus-detail/cursus-detail.component';
import { CursusNewComponent } from './component/cursus-new/cursus-new.component';
import { CursusEditComponent } from './component/cursus-edit/cursus-edit.component';

const routes: Routes = [
  { path: '', redirectTo: '/cursussen', pathMatch: 'full' },
  { path: 'detail/:id', component: CursusDetailComponent },
  { path: 'new', component: CursusNewComponent },
  { path: 'edit/:id', component: CursusEditComponent },
  { path: 'cursussen', component: CursusListComponent },
  { path: '**', redirectTo: '/cursussen' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
