import { Component, OnInit } from '@angular/core';
import { AdalService } from 'adal-angular4';
import { EnvService  } from '../env.service';

@Component({
  selector: 'my-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  authenticated = false;

  constructor(
    private service: AdalService,
    private env: EnvService
  ) {
    this.service.init(
      {
        instance: 'https://login.microsoftonline.com/',
        tenant: '3c281678-d72e-4f3e-86f6-88d54e69e803',
        clientId: env.clientId,
        extraQueryParameter: 'nux=1',
        cacheLocation: 'localStorage',
      }
    );
    this.service.userInfo.userName = 'Student@email.com';
    this.service.userInfo.profile.name = 'Student'
  }

  ngOnInit() {
    // Handle callback if this is a redirect from Azure
    this.service.handleWindowCallback();
    // const pattern = new RegExp('^(.*)@kza.nl$');
    // if (pattern.test(this.getEmail())) {
    //   this.authenticated = this.service.userInfo.authenticated;
    // }
    this.authenticated = true;
  }

  logOut() {
    this.authenticated = false;
    this.service.logOut();
  }

  logIn() {
    if (!this.service.userInfo.authenticated) {
      this.service.login();
    }
  }

  getUserName(): string {
    return this.service.userInfo.profile.name;
  }

  getEmail(): string {
    return this.service.userInfo.userName;
  }
}
