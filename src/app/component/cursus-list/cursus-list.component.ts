import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cursus } from '../../model/cursus';
import { CursusService } from '../../service/cursus.service';
import { AdalService } from 'adal-angular4';

@Component({
  selector: 'my-cursus-list',
  templateUrl: './cursus-list.component.html',
  styleUrls: ['./cursus-list.component.css']
})
export class CursusListComponent implements OnInit {
  email: string;
  cursussen: Cursus[];
  maanden: number[];
  jaren: number[];
  overgang: boolean;
  maandMetCursus: boolean;
  defaultFiltering = true;
  defaultSorteringProperty = 'datum';
  defaultSorteringOrder = false;
  maandenTekst = ['Januari', 'Februari', 'Maart', 'April', 'Mei', 'Juni',
    'Juli', 'Augustus', 'September', 'Oktober', 'November', 'December'];
  sorteringsOpties = ['attitude', 'datum', 'functieniveau', 'naam', 'status'];

  constructor(
    private adalService: AdalService,
    private router: Router,
    private cursusService: CursusService
  ) {}

  ngOnInit(): void {
    this.getLocalStorageProperties();
    this.getCursussen();
    this.email = this.adalService.userInfo.userName;
    this.maanden = [];
    this.jaren = [];

    this.maanden[0] = new Date().getMonth() + 1;
    this.jaren[0] = new Date().getFullYear();
    this.overgang = false;

    for (let i = 0; i < 10; i++) {
      this.maanden.push((this.maanden[0] + i) % 11 + 1);
      if ((this.maanden[0] + i) % 11 + 1 === 1) {
        this.overgang = true
      };
      if (this.overgang) {
        this.jaren.push(this.jaren[0] + 1);
      } else {
        this.jaren.push(this.jaren[0]);
      };
    };
  }

  getLocalStorageProperties(): void {
    if (this.getLocalStorageKey('filtering')) {
      this.defaultFiltering = (this.getLocalStorageKey('filtering') === 'true');
    };
    if (this.getLocalStorageKey('sorteringOrder')) {
      this.defaultSorteringOrder = (this.getLocalStorageKey('sorteringOrder') === 'true');
    };
    if (this.getLocalStorageKey('sorteringProperty')) {
      this.defaultSorteringProperty = this.getLocalStorageKey('sorteringProperty');
    };
  }

  changeSorteringOrder(): void {
    this.cursussen.reverse();
    this.defaultSorteringOrder = !this.defaultSorteringOrder;
    this.putLocalStorageKey('sorteringOrder', JSON.stringify(this.defaultSorteringOrder));
  }

  sortCursussenOrder(): void {
    if (this.defaultSorteringOrder) {
      this.cursussen.reverse();
    }
  }

  changeFiltering(): void {
    this.defaultFiltering = !this.defaultFiltering;
    this.putLocalStorageKey('filtering', JSON.stringify(this.defaultFiltering));
  }

  putLocalStorageKey(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  getLocalStorageKey(key: string): string {
    return localStorage.getItem(key);
  }

  sortCursussenProperty(propName: string): void {
    this.defaultSorteringProperty = propName;
    this.putLocalStorageKey('sorteringProperty', propName);
    if (propName === 'datum') {
      this.sortCursussenByDate();
    } else {
      console.log(this.cursussen)
      this.cursussen.sort((a, b) => {
        if (a[propName] < b[propName]) {
          return -1;
        }
        if (a[propName] > b[propName]) {
          return 1;
        }
        return 0;
      });
    }
  }

  sortCursussenByDate(): void {
    this.cursussen.sort((a, b) => {
      const aa =
        a.cursusdata[0].datum.substr(6, 4) +
        a.cursusdata[0].datum.substr(3, 2) +
        a.cursusdata[0].datum.substr(0, 2) +
        a.cursusdata[0].datum.substr(11, 2) +
        a.cursusdata[0].datum.substr(14, 2) +
        a.cursusdata[0].datum.substr(17, 2);
      const bb =
        b.cursusdata[0].datum.substr(6, 4) +
        b.cursusdata[0].datum.substr(3, 2) +
        b.cursusdata[0].datum.substr(0, 2) +
        b.cursusdata[0].datum.substr(11, 2) +
        b.cursusdata[0].datum.substr(14, 2) +
        b.cursusdata[0].datum.substr(17, 2);
      return aa < bb ? -1 : (aa > bb ? 1 : 0);
    });
  }

  maandHasCursus(maand: number): boolean {
    this.maandMetCursus = false;
    this.cursussen.forEach((cursus) => {
      if (+cursus.cursusdata[0].datum.substr(3, 2) ===
        this.maanden[maand] && +cursus.cursusdata[0].datum.substr(6, 4) === this.jaren[maand]) {
        this.maandMetCursus = true;
      }
    });
    return this.maandMetCursus;
  }

  getCursussen(): void {
    this.cursusService
      .getCursussen()
      .subscribe(
        cursussen => {
          this.cursussen = cursussen;
          this.getAanpassen();
          this.sortCursussenProperty(this.defaultSorteringProperty);
          this.sortCursussenOrder();
          this.getAangemeld();
        }
      )
  }

  goToNew(): void {
    this.router.navigate(['/new']);
  }

  gotoEdit(cursus: Cursus): void {
    this.router.navigate(['/edit', cursus.id]);
  }

  gotoDetail(cursus: Cursus): void {
    this.router.navigate(['/detail', cursus.id]);
  }

  deleteCursus(cursus: Cursus, event: any): void {
    event.stopPropagation();
    this.cursusService
      .deleteCursus(cursus)
      .subscribe(
        res => {this.cursussen = this.cursussen.filter(h => h !== cursus)}
      )
  }

   dateAfterToday(dateString): Boolean {
    const today = new Date();
    const [day, month, year] = dateString.split('-');
    const cursusDate = new Date(year, month - 1, day);
    return cursusDate > today;
  }

  getAanpassen(): void {
    this.cursussen.forEach(
      cursus => {
        if (this.email.toLocaleLowerCase() === 'wneve@kza.nl' || this.email.toLocaleLowerCase() === 'elinsen@kza.nl' ||
          this.email.toLocaleLowerCase() === 'chogerhuis@kza.nl') {
          cursus.aanpassen = false;
        } else {
          cursus.aanpassen = true;
        }
        cursus.cursusdocenten.forEach(
          docent => {
            if (docent.email.toLocaleLowerCase() === this.email.toLocaleLowerCase()) {
              cursus.aanpassen = false;
            }
          }
        );
      });
  }

  getAangemeld(): void {
    this.cursussen.forEach(
      cursus => {
         cursus.cursuscursisten.forEach(
          cursist => {
            if (cursist.email.toLocaleLowerCase() === this.email.toLocaleLowerCase()) {
              cursus.aangemeld = true;
            }
          }
        )
      })
  }
}
