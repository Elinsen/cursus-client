# Getting Started
[official docs](https://angular.io/).

## Prerequisites
Install `NodeJs` and `Angular cli`. Run `npm install` in the project root to install project dependencies.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production build
Run `ng build -prod` to build the project. The build artifacts will be stored in the `dist/` directory.
When on a Windows based system you should use `ng build-prod`.


## Creating an image
Run `docker build -t kzaconnected/cursus_client:latest .` to deploy the new `dist/` folder to docker hub.
Run `docker run -d -p 8080:8080 kzaconnected/cursus_client:latest` to run from docker hub. 
Run `docker run -d -p 8080:8080 -e APIURL=<url to backend service (forward slashes should be escaped e.g. http:\/\/)> kzaconnected/cursus_client:latest`.
Run `docker run -d -p 8080:8080 -e APIURL=<url to backend service (forward slashes should be escaped e.g. http:\/\/)> -e CLIENTID=<clientId> kzaconnected/cursus_client:latest`.

## Publish to docker hub
Run `docker push kzaconnected/cursus_client:latest` to deploy to docker hub.

