# Build a small nginx image
FROM nginx:1.14.2-alpine

# Remove default files
RUN rm -rf /usr/share/nginx/html/*
RUN rm -rf /etc/nginx/conf.d/*

# Copy config and static pages
COPY nginx.conf /etc/nginx/
COPY client-nginx.conf /etc/nginx/conf.d/
COPY /dist /usr/share/nginx/html

# Copy the EntryPoint
COPY ./entryPoint.sh /

# Set rights
RUN touch /var/run/nginx.pid && \
  chmod 777 -R /usr/share/nginx/ && \
  chmod 777 -R /var/cache/ && \
  chmod 777 -R /var/run/nginx.pid && \
  chmod g+s /var/cache/ && \
  chmod +x /entryPoint.sh

USER 1001

# Set entrypoint
ENTRYPOINT ["/entryPoint.sh"]

# Expose image
EXPOSE 8080
